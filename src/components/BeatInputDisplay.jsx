import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, createStyles } from '@material-ui/core/styles';
import { Card, Typography } from '@material-ui/core';

import { getPixelRatio, mod } from 'canvasUtils';

class BeatInputHere extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      taps: [],
      startTime: 0,
    };
    this.canvasRef = React.createRef();
    this.metronomeAudio = new Audio('metronome.wav');
    this.metronomeHighAudio = new Audio('metronome-high.wav');
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Typography variant="h6" align="center">
          Tap to the beat
        </Typography>
        <Card className={classes.root}>
          <canvas ref={this.canvasRef} className={classes.mainCanvas} />
        </Card>
      </div>
    );
  }

  componentDidMount() {
    /** @type {HTMLCanvasElement} */
    const canvas = this.canvasRef.current;
    const { bpm, timeSig } = this.props;

    // unfocus to prevent "space" triggering "next"
    document.activeElement.blur();

    // Both mousedown and touchstart are registered, since on mobile devices "mousedown" is not
    // triggered on touch.
    canvas.addEventListener('mousedown', this.makeTap);
    canvas.addEventListener('touchstart', this.onTouch);
    document.addEventListener('keydown', this.makeTap);

    const mountedTime = performance.now();
    const startTime = mountedTime + (60 / bpm) * 1000 * timeSig * 2;
    this.setState({ startTime });

    // Play metronome sound periodically
    this.metronomeHighAudio.play();
    let nthBeat = 0;
    this.metronomeInterval = setInterval(() => {
      nthBeat += 1;
      nthBeat %= timeSig;
      if (nthBeat % timeSig === 0) {
        this.metronomeHighAudio.play();
      } else {
        this.metronomeAudio.play();
      }
    }, (60 / bpm) * 1000);

    // Start canvas animation
    requestAnimationFrame(this.renderCanvas);
  }

  componentWillUnmount() {
    /** @type {HTMLCanvasElement} */
    const canvas = this.canvasRef.current;
    canvas.removeEventListener('mousedown', this.makeTap);
    canvas.removeEventListener('touchstart', this.onTouch);
    document.removeEventListener('keydown', this.makeTap);

    clearInterval(this.metronomeInterval);
  }

  onTouch = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.makeTap(e);
  };

  makeTap = (e) => {
    console.log(e);
    const timestamp = performance.now();

    this.setState(
      (state) => ({
        taps: state.taps.concat(timestamp),
      }),
      () => {
        const { onChange } = this.props;
        if (onChange) {
          onChange({
            startTime: this.state.startTime,
            taps: this.state.taps,
          });
        }
      }
    );
  };

  lastFrameTime = 0;
  shift = -200;
  renderCanvas = (currentTime) => {
    const { bpm, timeSig } = this.props;
    const { taps, startTime } = this.state;

    const spb = 60 / bpm;
    const mspb = 1000 * spb;

    /** @type {HTMLCanvasElement} */
    const canvas = this.canvasRef.current;
    if (!canvas) {
      console.log('canvas gone');
      return;
    }

    const context = canvas.getContext('2d');

    // Render depending on device DPI
    const ratio = getPixelRatio(context);

    // Resize canvas content with the canvas element
    const width = getComputedStyle(canvas).getPropertyValue('width').slice(0, -2);
    const height = getComputedStyle(canvas).getPropertyValue('height').slice(0, -2);
    canvas.width = width * ratio;
    canvas.height = height * ratio;

    // The horizontal line
    context.strokeStyle = '#7c7c7c';
    context.lineWidth = 1.5;
    context.beginPath();
    context.moveTo(0, Math.floor(canvas.height / 2));
    context.lineTo(canvas.width, Math.floor(canvas.height / 2));
    context.stroke();

    // Current bar is fixed
    const currentBarPos = canvas.width * 0.8;
    // Spacing is fixed
    const spaceBetweenBeats = 50 * ratio;
    const beatDotRadius = 5 * ratio;

    // Util function to convert time (ms) to signed position shift, relative to the current time bar
    const msToShift = (timeMs) => {
      return ((timeMs - currentTime) / mspb) * spaceBetweenBeats;
    };

    const prevBeatTime = currentTime - mod(currentTime - startTime, mspb);
    const prevBeatIndex = Math.floor((currentTime - startTime) / mspb);

    // Draw vertical lines (pass)
    let beatTime = prevBeatTime;
    let beatIndex = prevBeatIndex;
    while (currentBarPos + msToShift(beatTime) >= 0) {
      // Margins on top * bottom
      const trimLen = beatIndex % timeSig === 0 ? 0 : canvas.height * 0.16;
      const barX = currentBarPos + msToShift(beatTime);

      context.strokeStyle = '#bababa';
      context.lineWidth = 1;
      context.beginPath();
      context.moveTo(barX, trimLen);
      context.lineTo(barX, canvas.height - trimLen);
      context.stroke();

      beatTime -= mspb;
      beatIndex -= 1;
    }

    // Draw vertical lines (future)
    beatTime = prevBeatTime + mspb;
    beatIndex = prevBeatIndex + 1;
    while (currentBarPos + msToShift(beatTime) <= canvas.width) {
      // Margins on top * bottom
      const trimLen = beatIndex % timeSig === 0 ? 0 : canvas.height * 0.16;
      const barX = currentBarPos + msToShift(beatTime);

      context.strokeStyle = '#bababa';
      context.lineWidth = 1;
      context.beginPath();
      context.moveTo(barX, trimLen);
      context.lineTo(barX, canvas.height - trimLen);
      context.stroke();

      beatTime += mspb;
      beatIndex += 1;
    }

    // Current time vertical line
    context.strokeStyle = '#7c7c7c';
    context.lineWidth = 1;
    context.beginPath();
    context.moveTo(currentBarPos, 0);
    context.lineTo(currentBarPos, canvas.height);
    context.stroke();

    // Draw the dots produced by taps
    for (let i = taps.length - 1; i >= 0; i -= 1) {
      const tapTime = taps[i];
      const tapPos = currentBarPos + msToShift(tapTime);
      if (tapPos < 0 || tapPos > canvas.width) {
        break;
      }

      context.strokeStyle = '@7c7c7c';
      context.fillStyle = 'white';
      context.lineWidth = 3 * ratio;
      context.beginPath();
      context.arc(tapPos, canvas.height / 2, 8 * ratio, 0, 2 * Math.PI);
      context.fill();
      context.stroke();
    }

    // Draw gradient rect before the current time line
    const grad = context.createLinearGradient(currentBarPos, 0, canvas.width, 0);
    grad.addColorStop(0, '#c4c4c477');
    grad.addColorStop(1, '#c4c4c400');
    context.fillStyle = grad;
    context.fillRect(currentBarPos, 0, canvas.width, canvas.height);

    // Draw pulse effect around the current dot
    if (currentTime - prevBeatTime < 0.5 * mspb) {
      const completed = (currentTime - prevBeatTime) / (0.5 * mspb);
      const radius = Math.sin((completed * Math.PI) / 2) * 2 * beatDotRadius + 2 * beatDotRadius;

      context.beginPath();
      context.arc(currentBarPos, canvas.height / 2, radius, 0, 2 * Math.PI);
      context.fillStyle = `rgba(196, 196, 196, ${1 - Math.sin((completed * Math.PI) / 2)})`;
      context.fill();
    }

    // Draw the fixed dot on current time
    context.beginPath();
    context.arc(currentBarPos, canvas.height / 2, beatDotRadius, 0, 2 * Math.PI);
    context.fillStyle = '#7c7c7c';
    context.fill();

    this.lastFrameTime = currentTime;
    requestAnimationFrame(this.renderCanvas);
  };
}

BeatInputHere.propTypes = {
  bpm: PropTypes.number.isRequired,
  timeSig: PropTypes.number.isRequired,
  onChange: PropTypes.func,
};

const styles = createStyles({
  root: {
    height: '40vh',
  },
  mainCanvas: {
    height: '100%',
    width: '100%',
  },
});
export default withStyles(styles)(BeatInputHere);
