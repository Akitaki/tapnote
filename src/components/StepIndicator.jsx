import React from 'react';
import { Container, Stepper, Step, StepLabel } from '@material-ui/core';
import PropTypes from 'prop-types';

export const steps = {
  bpm: 0,
  beat: 1,
  output: 2,
};

const stepMsgs = ['BPM and Time Signature', 'Beat Input', 'Output'];

export class StepIndicator extends React.Component {
  render() {
    const { activeStep } = this.props;
    return (
      <Container maxWidth="sm">
        <Stepper
          activeStep={activeStep}
          alternativeLabel
          style={{
            backgroundColor: 'transparent',
          }}
        >
          {stepMsgs.map((msg) => (
            <Step key={msg}>
              <StepLabel>{msg}</StepLabel>
            </Step>
          ))}
        </Stepper>
      </Container>
    );
  }
}

StepIndicator.propTypes = {
  activeStep: PropTypes.oneOf([0, 1, 2]),
};
