// Binary search closest element from `num` in `arr`
function closest(num, arr) {
  var mid;
  var lo = 0;
  var hi = arr.length - 1;
  while (hi - lo > 1) {
    mid = Math.floor((lo + hi) / 2);
    if (arr[mid] < num) {
      lo = mid;
    } else {
      hi = mid;
    }
  }
  if (num - arr[lo] <= arr[hi] - num) {
    return lo;
  }
  return hi;
}

/**
 * Convert array of timestamps to abc notation
 * @param {number[]} taps The timestamps
 * @param {number} startTime Timestamp of the starting time, used for measure alignment
 * @param {number} bpm Beats per minute
 * @param {number} beatsPerMeasure
 * @param {number} pointsPerBeat Number of divisions per beat; must be non-negative power of 2
 * @param {number} unit Note variant used as one beat
 * @returns {string}
 */
export function convert(taps, startTime, bpm, beatsPerMeasure, pointsPerBeat, unit) {
  if (taps.length === 0) {
    // TODO: return empty abc string with correct time signature
    let header = [
      'X: 1',
      `M: ${unit}/${beatsPerMeasure}`,
      `Q: 1/${unit}=${bpm}`,
      `L: 1/${unit}`,
      'K: C',
    ].join('\n');
    return header;
  }
  // Beats per millisecond / milliseconds per beat
  const mspb = (60 / bpm) * 1000;

  // Original taps, normalized as fractional beats from startTime
  const normalizedTaps = taps.map((tapTime) => (tapTime - startTime) / mspb);

  // Measure of the first tap, NOT rounded up to next measure
  const firstPossibleMeasure = Math.floor(normalizedTaps[0] / beatsPerMeasure);
  // Measure of the last tap, rounded up to next measure
  // (it can be on the very last instant of the previous measure, which should be counted as
  // to be in the next measure)
  const lastPossibleMeasure = Math.ceil(
    normalizedTaps[normalizedTaps.length - 1] / beatsPerMeasure
  );

  // Time points for the taps to "snap" to (i.e. find the nearest from)
  let snapPoints = [];
  for (let measure = firstPossibleMeasure; measure <= lastPossibleMeasure; measure += 1) {
    for (let beat = 0; beat < beatsPerMeasure; beat += 1) {
      for (let point = 0; point < pointsPerBeat; point += 1) {
        // Time to measure start + measure start to beat start + beat start to snap point
        const snap =
          startTime +
          measure * beatsPerMeasure * mspb +
          beat * mspb +
          (point / pointsPerBeat) * mspb;
        snapPoints.push(snap);
      }
    }
  }

  // Taps snapped to snap points
  let snappedTaps = taps.map((tap) => {
    const closestSnapPoint = closest(tap, snapPoints);
    return closestSnapPoint;
  });

  // Deduplicate to avoid notes with zero duration
  snappedTaps = [...new Set(snappedTaps)];

  // Note objects, without considering non-representable lengths such as 5
  let notes = snappedTaps
    .map((tapStart, index, taps) => {
      const nextTap = taps[index + 1];
      // Calculate the measures where the start and end of the tap are
      const startMeasure = Math.floor(tapStart / (beatsPerMeasure * pointsPerBeat));
      let tapEnd = null;
      if (nextTap) {
        tapEnd = nextTap - 1;
      } else {
        tapEnd = (startMeasure + 1) * (beatsPerMeasure * pointsPerBeat) - 1;
      }
      const endMeasure = Math.floor(tapEnd / (beatsPerMeasure * pointsPerBeat));

      if (startMeasure === endMeasure) {
        // Case I: not crossing multiple measures
        let notes = [
          {
            duration: tapEnd - tapStart + 1,
            prefixTie: false,
            inMeasure: startMeasure,
            case1: true,
            tapEnd: tapEnd,
          },
        ];
        if (tapEnd === (startMeasure + 1) * (beatsPerMeasure * pointsPerBeat) - 1) {
          notes.push({
            sep: true,
          });
        }
        return notes;
      } else {
        // Case I: note crosses multiple measures
        let notes = [
          {
            duration: (startMeasure + 1) * (beatsPerMeasure * pointsPerBeat) - tapStart,
            prefixTie: false,
            inMeasure: startMeasure,
            case2: true,
          },
          { sep: true },
        ];
        for (let measure = startMeasure + 1; measure < endMeasure; measure += 1) {
          notes.push({
            duration: beatsPerMeasure * pointsPerBeat,
            prefixTie: true,
            inMeasure: measure,
          });
          notes.push({
            sep: true,
          });
        }
        notes.push({
          duration: tapEnd - endMeasure * (beatsPerMeasure * pointsPerBeat) + 1,
          prefixTie: true,
          inMeasure: endMeasure,
        });
        if (tapEnd === (endMeasure + 1) * (beatsPerMeasure * pointsPerBeat) - 1) {
          notes.push({
            sep: true,
          });
        }
        return notes;
      }
    })
    .flat();

  // Construct a list of lengths representable by a single note (with optional dots)
  let decompTable = [];
  for (let i = 1; i <= beatsPerMeasure * pointsPerBeat; i *= 2) {
    let prevRow = decompTable[decompTable.length - 1] || [];
    let thisRow = [i];
    for (let num of prevRow) {
      thisRow.push(i + num);
    }
    decompTable.push(thisRow);
  }
  let representableLengths = new Set(decompTable.flat());

  // Decompose notes with lengths that are not representable by a single note
  // (e.g. 1.25 times the length of a quarter note should be one quarter note + one sixteenth note)
  let decomposedNotes = notes
    .map((note) => {
      if (note.sep || representableLengths.has(note.duration)) {
        return [note];
      } else {
        let notes = [];
        let remainingDuration = note.duration;
        let isFirstNote = true;

        // Maximum note length that fits within a single measure
        const maxUnitInMeasure = Math.pow(
          2,
          Math.floor(Math.log2(beatsPerMeasure * pointsPerBeat))
        );

        // Larger units has higher priority (e.g. 8, 4, 2, 1)
        for (let i = maxUnitInMeasure; i >= 1; i /= 2) {
          if (remainingDuration >= i) {
            let newNote = { ...note };

            // Decomposed length
            newNote.duration = i;

            if (isFirstNote) {
              // The first note inherits prefixTie from the original note
              newNote.prefixTie = note.prefixTie;
            } else {
              // Other notes should be tied to its predecessor
              newNote.prefixTie = true;
            }
            notes.push(newNote);

            remainingDuration -= i;
            isFirstNote = false;
          }
        }
        return notes;
      }
    })
    .flat();

  let header = [
    'X: 1',
    `M: ${unit}/${beatsPerMeasure}`,
    `Q: 1/${unit}=${bpm}`,
    `L: 1/${unit}`,
    'K: C',
  ].join('\n');
  let body = decomposedNotes
    .map((note) => {
      if (note.sep) {
        return '|';
      } else {
        let prefix = note.prefixTie ? '-' : '';
        return `${prefix}G${note.duration}/${pointsPerBeat}`;
      }
    })
    .join('');
  return header + '\n' + body + ']';
}
